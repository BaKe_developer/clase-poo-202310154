﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Empleado
{
    public class EmpleadoporHoras : Empleados
    {
        public override void CalculaSalario(double SA, double SH, int HT)
        {
            if(HT<=40)
            {
                Sueldo = SH * HT;
            }
            if(HT>40)
            {
                Sueldo = 40 * SH + (HT - 40) * SH * 1.5;
            }
        }
    }
}