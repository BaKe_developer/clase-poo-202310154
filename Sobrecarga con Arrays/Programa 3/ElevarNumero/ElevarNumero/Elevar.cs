﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevarNumero
{
    class Elevar
    {
        public void elevacion(double a)
        {
            Console.WriteLine("Antes " + a);
            a = Math.Sqrt(a);
            Console.WriteLine("Despues " + a);
        }

        public void elevacion(float a)
        {
            Console.WriteLine("Antes " + a);
            a = a * a;
            Console.WriteLine("Despues " + a);
        }
        public void elevacion(int a)
        {
            Console.WriteLine("Antes " + a);
            a = a * a;
            Console.WriteLine("Despues " + a);
        }
    }
}
