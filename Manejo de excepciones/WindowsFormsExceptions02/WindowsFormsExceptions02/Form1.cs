﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsExceptions02
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalcula_Click(object sender, EventArgs e)
        {
            try
            {
                double result = 0;

                int numero1 = int.Parse(txt1.Text);
                int numero2 = int.Parse(txt2.Text);
                result = numero1 + numero2;
                lblResultado.Text = Convert.ToString(result);
            }
            catch(OverflowException ex)
            {
                MessageBox.Show("Digito demasiado grande");
            }
            catch(FormatException ex)
            {
                MessageBox.Show("Ingresa un valor correcto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                MessageBox.Show("Termino del programa");
            }

        }
    }
}
