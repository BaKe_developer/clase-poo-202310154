﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa_3
{
    interface Ave
    {
        public void volar()
        {
            Console.WriteLine("Está volando...");
        }

        public void Beber()
        {
            Console.WriteLine("Está tomando agua...");
        }
    }
}
