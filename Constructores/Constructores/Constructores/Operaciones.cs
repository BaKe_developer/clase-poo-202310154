﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructores
{
    class Operaciones
    {
        private int multa;

        public Operaciones(int multa)
        {
            this.multa = multa;
        }

        public int CalcularMulta()
        {
            int resultado = 0;

            switch (this.multa)
            {
                case 0:
                    return 180;
                    break;

                case 1:
                    return 500;
                    break;

                case 2:
                    return 200;
                    break;

                case 3:
                    return 500;
                    break;


                default:
                    resultado = 0;
                    break;
            }
            return resultado;
        }
    }
}