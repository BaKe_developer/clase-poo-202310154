﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodosVirtuales
{
    class Program
    {
        static void Main(string[] args)
        {
            Triangulo objT = new Triangulo();
            objT.CalculaArea(5, 7);

            Cuadrado objC = new Cuadrado();
            objC.CalculaArea(5, 4);

            Console.ReadKey();
        }
    }
}
