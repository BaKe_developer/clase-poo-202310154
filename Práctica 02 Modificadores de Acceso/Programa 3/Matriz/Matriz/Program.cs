﻿using System;

namespace Matriz
{
    class Program
    {
        static void Main(string[] args)
        {
            MatrizNxM obj = new MatrizNxM();
            obj.LeerDatos();
            obj.MostrarDatos();
        }
    }
}
