﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Práctica_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_Clase_Click(object sender, EventArgs e)
        {
            tv tele = new tv();

            tele.setTamano(24);
            int tamanoTele = tele.getTamano();

            tele.setVolumen(17);
            int volumenTele = tele.getVolumen();

            tele.setColor("Negra");
            string colorTele = tele.getColor();

            tele.setBrillo(40);
            int brilloTele = tele.getBrillo();

            tele.setContraste(35);
            int contrasteTele = tele.getContraste();

            MessageBox.Show("El tamaño de la tv es de " + tamanoTele);
            MessageBox.Show("El volumen de la tv es de " + volumenTele);
            MessageBox.Show("El color de la tv es " + colorTele);
            MessageBox.Show("El brillo de la tv es de " + brilloTele);
            MessageBox.Show("EL contraste de la tv es de " + contrasteTele);
            MessageBox.Show("Gracias por usar la TV, bye");
        }
    }
}
