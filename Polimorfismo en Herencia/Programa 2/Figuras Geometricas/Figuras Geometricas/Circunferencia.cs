﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figuras_Geometricas
{
    public class Circunferencia : Operaciones
    {
        public override void CalcularArea()
        {
            area = Math.PI * (radio * radio);
        }
    }
}
